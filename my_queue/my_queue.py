"""
Programming for linguists

Implementation of the data structure "Queue"
"""

from typing import Iterable


class MyQueue:
    """
    Queue Data Structure
    """

    def __init__(self, data: Iterable = (), max_size: int = 0):
        pass

    def put(self, element):
        """
        Add the element ‘element’ at the end of my_queue
        :param element: element to add to my_queue
        """

    def get(self):
        """
        Remove and return an item from my_queue
        """

    def empty(self) -> bool:
        """
        Return whether my_queue is empty or not
        :return: True if my_queue does not contain any elements.
                 False if the my_queue contains elements
        """

    def full(self) -> bool:
        """
        Return whether my_queue is full or not
        :return: True if my_queue is full.
                 False if my_queue is not full
        """

    def size(self) -> int:
        """
        Return the number of elements in my_queue
        :return: Number of elements in my_queue
        """

    def capacity(self) -> int:
        """
        Return the maximal size of my_queue
        :return: Maximal size of my_queue
        """
