"""
Programming for linguists

Implementation of the data structure "Stack"
"""

from typing import Iterable


class MyStack:
    """
    Stack Data Structure
    """

    def __init__(self, data: Iterable = None):
        pass

    def push(self, element):
        """
        Add the element ‘element’ at the top of my_stack
        :param element: element to add to my_stack
        """

    def pop(self):
        """
        Delete the element on the top of my_stack
        """

    def top(self):
        """
        Return the element on the top of my_stack
        :return: the element that is on the top of my_stack
        """

    def size(self) -> int:
        """
        Return the number of elements in my_stack
        :return: Number of elements in my_stack
        """

    def empty(self) -> bool:
        """
        Return whether my_stack is empty or not
        :return: True if my_stack does not contain any elements
                 False if my_stack contains elements
        """
